<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


// define('WP_HOME','https://localhost/aa');
// define('WP_SITEURL','https://localhost/aa');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'adviser2_adviseralliance');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=3dle@d$Z#H@hZ0*Nf.F_-YJ<$w`(C)(/:$y0%AoF:mr@0Ia9AQ+f26jNBZK9=#c');
define('SECURE_AUTH_KEY',  '!#ShM7@g*6B~|hKz/*oUl)Y|}%c?|f{H|8fXGkPrs#*|vv&D|kBr%YSXIC+/}H?p');
define('LOGGED_IN_KEY',    ' &^~}u=z*1Jcee0+x2e-q5XQ:LHe1KaV(Yw:aOS|U@t[fw1u>N[&$cU6|r)?9hoc');
define('NONCE_KEY',        '7M8[~{d5eruymvsf,8|P@CR|O:Xaq!&GN, &)dZX<LOp~t:;^+Q*-od1-i5[]O$n');
define('AUTH_SALT',        'b(mYeo|6rWL2/;^!U4i-;fn2Cu3>Xk1i!U?]kQ2:@c!2{b:ifMnl[E[Cf7Xg*]zO');
define('SECURE_AUTH_SALT', 'm94CL]MG8/l*%Nc@Qv{eyPb27?Rt]Q1Il+2$0$c]-912yWsh[e.r|4 Ajk]aPe8*');
define('LOGGED_IN_SALT',   'VJz}}HF02lE2Wx!LEo$;QbFt{Y3z8u.0-H_z~5T*j =.p#%Bh*}j$>q;xLP|:7#^');
define('NONCE_SALT',       ':+0PZj9jmhUYc;Ht|>6zLti$nS:pe{ yqhOgUQ7+%U,Ha,{3<(v!(Qs|Sp*|obCn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ppp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
