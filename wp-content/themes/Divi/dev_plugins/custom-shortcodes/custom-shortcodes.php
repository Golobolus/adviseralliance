<?php

	/*==== Allow Shortcodes in text widget ====*/

	add_filter('widget_title', 'do_shortcode');

	add_filter('widget_text', 'do_shortcode');

	/*==== Allow Shortcodes in text widget END=*/



	/*==== Custom Shortcodes ====*/

	//---- Header ----//

	add_shortcode('header_contact', 'header_contact');

	function header_contact(){

		return et_get_option('dev_custom-header_contact');

	}



	add_shortcode('header_tagline', 'header_tagline');

	function header_tagline(){

		return et_get_option('dev_custom-header_tagline');

	}



	//---- Footer ----//

	add_shortcode('Render_footer_copyright_details', 'Render_footer_copyright_details');

	function Render_footer_copyright_details(){

		/*Init*/

		$returnContent = '';



		/*Collect data*/

		$footer_copyright = et_get_option('dev_custom-footer_copyright');

		$footer_acn = et_get_option('dev_custom-footer_acn');

		$footer_company_text = et_get_option('dev_custom-footer_company_text');



		/*Defaults*/

		if($footer_copyright <> '') $footer_copyright = 'Pay Per Plan Pty. Ltd.';

		if($footer_acn <> '') $footer_copyright = 'Copyright &copy; 2014.';



		/*Build*/

		$returnContent .= "<span>{$footer_company_text}</span>";

		$returnContent .= "<span>{$footer_copyright}</span>";

		$returnContent .= "<span class='dark'>ACN {$footer_acn}</span>";



		return $returnContent;

	}



	add_shortcode('Render_footer_addresses', 'Render_footer_addresses');

	function Render_footer_addresses(){

		/*Init*/

		$returnContent = '';

		$addresses = array();



		/*Collect data*/

		$footer_address1 = et_get_option('dev_custom-footer_address1');

		$footer_address2 = et_get_option('dev_custom-footer_address2');

		$footer_address3 = et_get_option('dev_custom-footer_address3');



		/*Collect Addresses*/

		if($footer_address1 != '') $addresses[] = $footer_address1;

		if($footer_address2 != '') $addresses[] = $footer_address2;

		if($footer_address3 != '') $addresses[] = $footer_address3;



		/*Build*/

		$returnContent .= '<div class="footer-addresses">';

		foreach ($addresses as $key => $address) {

			/*Dark color if even row*/

			$class = '';

			if($key % 2 == 0) $class = 'dark';



			/*Append*/

			$returnContent .= "<span class='{$class}'>{$address}</span>";

		}

		$returnContent .= '</div>';



		return $returnContent;

	}



	//---- Section 1----//

	add_shortcode('section1_title', 'section1_title');

	function section1_title(){

		return get_the_title(291);

		//return et_get_option('dev_custom-section1_title');

	}



	add_shortcode('section1_bold_phrase', 'section1_bold_phrase');

	function section1_bold_phrase(){

		return get_field('section_bold', 291);

		//return et_get_option('dev_custom-section1_bold_phrase');

	}



	add_shortcode('section1_text', 'section1_text');

	function section1_text(){
		$originalText = get_field('content_text', 291);
		$trimmed = strrev(preg_replace('~^(.*?)'.strrev('</p>').'~s', '$1'.strrev('...'), strrev($originalText), 1));

		if(do_shortcode('[section1_text_More_URl]') != ''){
			$trimmed = $trimmed . do_shortcode('<a href="[section1_text_More_URl]" target="[parse_link_target][section1_text_More_URl][/parse_link_target]"> More</a></p>');
		}
		return $trimmed;

	}



	add_shortcode('section1_text_More_URl', 'section1_text_More_URl');

	function section1_text_More_URl(){

		return get_field('more_target_url', 291);

	}



	add_shortcode('section1_iconset_title', 'section1_iconset_title');

	function section1_iconset_title(){

		return get_field('section_1_icon-set_title', 291);

	}



	add_shortcode('section1_iconset_text', 'section1_iconset_text');

	function section1_iconset_text(){

		return get_field('section_1_icon-set_text', 291);

	}



	//---- Section 2 ----//

	add_shortcode('section2_title', 'section2_title');

	function section2_title(){

		return get_the_title(292);

		//return et_get_option('dev_custom-section2_title');

	}



	add_shortcode('section2_bold_phrase', 'section2_bold_phrase');

	function section2_bold_phrase(){

		return get_field('section_bold', 292);

		//return et_get_option('dev_custom-section2_bold_phrase');

	}



	add_shortcode('section2_text', 'section2_text');

	function section2_text(){

		return get_field('content_text', 292);

	}



	add_shortcode('section2_iconset_title', 'section2_iconset_title');

	function section2_iconset_title(){

		if(get_field('section_2_mini-slider_title', 292)){
			return '<h4 style="color: #93000c;">' . get_field('section_2_mini-slider_title', 292) . '</h4>';
		}

		return "";

	}



	add_shortcode('section2_iconset_text', 'section2_iconset_text');

	function section2_iconset_text(){

		return get_field('section_2_mini-slider_text', 292);

	}



	//---- Section 3 ----//

	add_shortcode('section3_title', 'section3_title');

	function section3_title(){

		return get_the_title(293);

	}



	add_shortcode('section3_bold_phrase', 'section3_bold_phrase');

	function section3_bold_phrase(){

		return get_field('section_bold', 293);

		//return et_get_option('dev_custom-section3_bold_phrase');

	}



	add_shortcode('section3_text', 'section3_text');

	function section3_text(){
		return get_field('content_text', 293);
	}



	add_shortcode('section3_lower_title', 'section3_lower_title');

	function section3_lower_title(){

		if(get_field('section_3_lower_title', 293)){
			return '<h4 style="text-align: center;">'.get_field('section_3_lower_title', 293)."</h4>";
		}
		return "";

	}



	add_shortcode('section3_lower_text', 'section3_lower_text');

	function section3_lower_text(){

		return get_field('section_3_lower_text', 293);

	}




	//---- Section 3 ----//
	
	add_shortcode('section4_title', 'section4_title');

	function section4_title(){

		return get_the_title(294);

		//return et_get_option('dev_custom-section4_title');

	}



	add_shortcode('section4_bold_phrase', 'section4_bold_phrase');

	function section4_bold_phrase(){

		return get_field('section_bold', 294);

		//return et_get_option('dev_custom-section4_bold_phrase');

	}



	add_shortcode('section4_text', 'section4_text');

	function section4_text(){

		return get_field('content_text', 294);

	}



	add_shortcode('section4_button_text', 'section4_button_text');

	function section4_button_text(){

		return get_field('section_4_button_text', 294);

		//return et_get_option('dev_custom-section4_button_text');

	}



	add_shortcode('section4_button_page', 'section4_button_page');

	function section4_button_page(){

		return get_field('section_4_button_landing_page', 294);

		//return get_page_link(et_get_option('dev_custom-section4_button_page'));

	}





	/*Section BG opions*/

	add_shortcode('section1_bg', 'section1_bg');

	function section1_bg(){

		return et_get_option('dev_custom-section1_bg');

	}
	/*==== Custom Shortcodes END=*/

?>
<?php

	/*==== FAQ Shortcodes ====*/

	add_shortcode('Render_FAQ', 'Render_FAQ');

	function Render_FAQ(){

		/*Settings*/

		$columns_per_row = 3;



		/*holders*/

		$returnContent = '';



		/*Get FAQ items*/

		if(isset($wp_query)) {

			$temp = $wp_query;

			$wp_query = null;

		}

		else{

			$temp = null;

		}	

		$wp_query = new WP_Query(); 

		$wp_query->query('posts_per_page=-1&post_type=cpt_faq_item&orderby=menu_order&order=ASC'); 



		/*Init*/

		$current_item_index = 1;



		/*==== Start populating returnContent ====*/

		$returnContent .= '<div class="FAQ_wrap">';

			while ($wp_query->have_posts()) : $wp_query->the_post();

				/*Check if First in Row*/

				if($current_item_index % $columns_per_row == 1){

					$returnContent .= '<div class="FAQ_row">';

				}

				/*Fetch FAQ details*/

				$FAQ_ID = get_the_ID();

				$FAQ_title = get_the_title();

				$FAQ_content = get_field('faq_answer', get_the_ID());



				/*==== The FAQ item ====*/  //[one_third][learn_more caption="Caption goes here TSET" state="open"] TEST[/learn_more][/one_third]

				$returnContent .= '[one_third]';

					$returnContent .= "[learn_more caption=\"{$FAQ_title}\" state=\"open\"]";

						$returnContent .= $FAQ_content;

					$returnContent .= "[/learn_more]";

				$returnContent .= '[/one_third]';

				/*==== The FAQ item END=*/



				/*End row if last*/

				if($current_item_index % $columns_per_row == 0){

					$returnContent .= '</div><!-- FAQ_row -->';

				}

				$current_item_index++;

			endwhile;



			/*Catch if last row did not end with 3 columns*/

			if($current_item_index % $columns_per_row == 0){

				$returnContent .= '</div><!-- FAQ_row -->';

			}

		$returnContent .= '</div>';

		/*==== Start populating returnContent END=*/





		$wp_query = null; 

		$wp_query = $temp;





		/*End and return rendered*/

		return do_shortcode($returnContent);

	}



	/*==== FAQ Shortcodes END=*/

?>
<?php

	/*==== About the Team ====*/

	add_shortcode('Render_AboutTeam', 'Render_AboutTeam');

	function Render_AboutTeam(){

		/*holders*/

		$returnContent = '';



		/*Get Team members*/

		if(isset($wp_query)) {

			$temp = $wp_query;

			$wp_query = null;

		}

		else{

			$temp = null;

		}		$wp_query = new WP_Query();

		$wp_query->query('posts_per_page=-1&post_type=cpt_team_member&orderby=menu_order&order=ASC');



		/*Runtime*/

		$returnContent .= '<div class="team_members">';

		while ($wp_query->have_posts()) : $wp_query->the_post();



			/*Collect requireed data*/

			$member_ID = get_the_ID();

			$member_name = get_the_title();

			$member_photo = get_field('member_photo', $member_ID);

			$member_designtation = get_field('member_designtation', $member_ID);

			$member_details = get_field('member_details', $member_ID);



			$returnContent .= '[author]';

				$returnContent .= "[author_image timthumb='off']{$member_photo}[/author_image]";

				$returnContent .= "[author_info]";

					$returnContent .= "<h3 class='member-name'>{$member_name}</h3>";

					$returnContent .= "<p class='member-designation'>{$member_designtation}</p>";

					$returnContent .= "$member_details";



					/*===== Social Icons =====*/

					/*Get Links*/

					$facbook = get_field('member_facebook_url', $member_ID);

					$twitter = get_field('member_twitter_url', $member_ID);;

					$google = get_field('member_google_url', $member_ID);;

					$linkedin = get_field('member_linkedin_url', $member_ID);;



					/*build Icons*/

					$returnContent .= "<div class='social_icons'>";

						if($facbook <> '') $returnContent .= "<a href='{$facbook}' class='facebook'><img src='[get_imageLoc_fb]'/></a>";

						if($twitter <> '') $returnContent .= "<a href='{$twitter}' class='facebook'><img src='[get_imageLoc_t]'/></a>";

						if($google <> '') $returnContent .= "<a href='{$google}' class='facebook'><img src='[get_imageLoc_g]'/></a>";

						if($linkedin <> '') $returnContent .= "<a href='{$linkedin}' class='facebook'><img src='[get_imageLoc_in]'/></a>";

					$returnContent .= "</div>";

					/*===== Social Icons END=*/

				$returnContent .= "[/author_info]";

			$returnContent .= '[/author]';

		endwhile;

		$returnContent .= '</div>';



		$wp_query = null; 

		$wp_query = $temp;



		/*End and return rendered*/

		return do_shortcode($returnContent);

	}

?>
<?php

	add_shortcode('Render_Map', 'Render_Map');

	function Render_Map($atts){

		/*holders*/

		$returnContent = '';



		/*Runtime*/

		$returnContent .= '<div class="custom-shortcode-map">';

			/*Add address if specified*/

			if (isset($atts['add']) && !empty($atts['add'])) {

				$returnContent .= "<div class='map_address'>{$atts['add']}</div>";

			}

			$returnContent .= '<div class="acf-map">';

				$returnContent .= "<div class='marker' data-lat='{$atts['lat']}' data-lng='{$atts['lng']}'></div>";

			$returnContent .= '</div>';

		$returnContent .= '</div>';



		/*End and return rendered*/

		return do_shortcode($returnContent);

	}

?>
<?php

	/*==== Contact Form ====*/

	add_shortcode('Render_Map_for_contact', 'Render_Map_for_contact');

	function Render_Map_for_contact(){

		/*Get Contact map settings*/

		$map_details = get_field('map_location', 508);



		/*End and return rendered*/

		return do_shortcode("<div class='contact_map'>[Render_Map lat='{$map_details['lat']}' lng='{$map_details['lng']}' add='{$map_details['address']}']</div>");

	}





	add_shortcode('contact_form_address', 'contact_form_address');

	function contact_form_address(){

		/*Get Contact map settings*/

		$map_details = get_field('map_location', 508);



		/*End and return rendered*/

		return  $map_details['address'];

	}





	add_shortcode('contact_form_phone', 'contact_form_phone');

	function contact_form_phone(){

		return get_field('phone', 508);

	}





	add_shortcode('contact_form_email', 'contact_form_email');

	function contact_form_email(){

		return get_field('email_address', 508);

	}

?>
<?php

	/*==== Testimonials Page ====*/

	add_shortcode('Render_TestimonialsList', 'Render_TestimonialsList');

	function Render_TestimonialsList(){

		/*holders*/

		$returnContent = '';



		/*Get Team members*/

		if(isset($wp_query)) {

			$temp = $wp_query;

			$wp_query = null;

		}

		else{

			$temp = null;

		}		$wp_query = new WP_Query();

		$wp_query->query('posts_per_page=-1&post_type=cpt_testimonial&orderby=menu_order&order=ASC');



		/*Runtime*/

		$returnContent .= '<div class="testimonials_list">';

		while ($wp_query->have_posts()) : $wp_query->the_post();



			/*Collect requireed data*/

			$testimonial_ID = get_the_ID();

			$author_name = get_field('testimonial_author', $testimonial_ID);

			$author_designation = get_field('author_designation', $testimonial_ID);

			$author_company = get_field('author_company', $testimonial_ID);

			$author_photo = get_field('author_photo', $testimonial_ID);

			$testimonial_message = get_the_content();



			/*Build*/

			$returnContent .= '[author]';

				$returnContent .= "[author_image timthumb='off']{$author_photo['sizes']['thumbnail']}[/author_image]";

				$returnContent .= "[author_info]";

					$returnContent .= $testimonial_message;

					$returnContent .= "<h4 class='testimonial-author'>{$author_name}</h4>";

					$returnContent .= "<p class='testimonial-details'>{$author_designation}, <span class='author-company'>{$author_company}</span></p>";

				$returnContent .= "[/author_info]";

			$returnContent .= '[/author]';

		endwhile;

		$returnContent .= '</div>';



		$wp_query = null; 

		$wp_query = $temp;



		/*End and return rendered*/

		return do_shortcode($returnContent);

	}

?>
<?php

	/*==== Hompage Section 1 ====*/

	add_shortcode('Render_enjoyList', 'Render_enjoyList');

	function Render_enjoyList(){

		/*holders*/

		$returnContent = '';



		/*Collect requireed data*/

		$icons = get_field('icon-set', 291);



		/*Build*/

		if($icons){

			$returnContent .= '<ul class="enjoy-list">';

				foreach($icons as $icon){

					if(is_externalLink($icon['target_link'])) $target = "_blank";

					else $target = "_self";

					$returnContent .= '<li>';

						$returnContent .= "<a href='{$icon['target_link']}' target='{$target}'>";

							/*Crete Title*/
							$title = preg_replace('#<[^>]+>#', ' ', $icon['icon_text']);

							$returnContent .= "<img class='alignnone size-full' src='{$icon['icon']['sizes']['thumbnail']}' title='{$title}' alt='{$icon['icon_text']}' width=\"60\" height=\"60\" />";

							$returnContent .= "<strong>{$icon['icon_text']}</strong>";

						$returnContent .= '</a>';

					$returnContent .= '</li>';


				}

			$returnContent .= '</ul>';

		}

		/*End and return rendered*/

		return do_shortcode($returnContent);

	}

?>
<?php

	/*For About The Founder*/

	add_shortcode('get_the_founder_name', 'get_the_founder_name');

	function get_the_founder_name(){

		return  get_field('founder_name', 607);

	}

	add_shortcode('get_the_founder_photo', 'get_the_founder_photo');

	function get_the_founder_photo(){

		$photo = get_field('founder_photo', 607);

		return $photo['sizes']['medium'];

	}

	add_shortcode('get_the_founder_bio', 'get_the_founder_bio');

	function get_the_founder_bio(){

		return  get_field('founder_bio', 607);

	}

	add_shortcode('get_the_founder_details', 'get_the_founder_details');

	function get_the_founder_details(){

		return  get_field('founder_details', 607);

	}

	add_shortcode('get_the_founder_facebook', 'get_the_founder_facebook');

	function get_the_founder_facebook(){

		return  get_field('facebook_url', 607);

	}

	add_shortcode('get_the_founder_twitter', 'get_the_founder_twitter');

	function get_the_founder_twitter(){

		return  get_field('twitter_url', 607);

	}

	add_shortcode('get_the_founder_google', 'get_the_founder_google');

	function get_the_founder_google(){

		return  get_field('google+_url', 607);

	}

	add_shortcode('get_the_founder_linked', 'get_the_founder_linked');

	function get_the_founder_linked(){

		return  get_field('linkedin_url', 607);

	}



?>
<?php



	/*Clients served*/

	add_shortcode('Render_clients_served_list', 'Render_clients_served_list');

	function Render_clients_served_list(){

		/*Settings*/

		$columns_per_row = 3;



		/*holders*/

		$returnContent = '';



		/*Get clients*/

		if(isset($wp_query)) {

			$temp = $wp_query;

			$wp_query = null;

		}

		else{

			$temp = null;

		}		$wp_query = new WP_Query();

		$wp_query->query('posts_per_page=-1&post_type=cpt_client&orderby=menu_order&order=ASC');



		/*==== Runtime ====*/

		/*Init*/

		$current_item_index = 1;

		$returnContent .= '<div class="client_list">';

		while ($wp_query->have_posts()) : $wp_query->the_post();



			/*Collect requireed data*/

			$client_id = get_the_ID();

			$client_name = get_the_title();

			$client_logo = get_field('client_logo', $client_id);

			$client_tagline = get_field('tagline', $client_id);

			$client_website = get_field('client_website_url', $client_id);

			$link_class = '';



			/*Check if no link specified*/

			if($client_website == '') {
				$client_website = 'javascript:void(0)';
				$target = '_self';
			}
			else {
				$client_website = convert_link($client_website);
				$target = '_blank';
			}



			/*Build*/

			if($current_item_index % $columns_per_row == 0) $returnContent .= '[one_third_last]';

			else $returnContent .= "[one_third]";

				$returnContent .= "<a href='{$client_website}' target='{$target}'>";

					$returnContent .= "<div class='client_logo'>";

						$returnContent .= "<div class='client_logo_wrap'>";

							$returnContent .= "<div class='client_logo_wrap_inner'>";

								$returnContent .= "<img class='alignnone size-full' src='{$client_logo['sizes']['medium']}' alt='{$client_name}' />";

							$returnContent .= "</div>";

						$returnContent .= "</div>";

					$returnContent .= "</div>";

					$returnContent .= "<div class='client_text'>";

						$returnContent .= "<strong>{$client_name}</strong>";

						$returnContent .= "<span class='client-tagline'>{$client_tagline}</span>";

					$returnContent .= "</div>";

				$returnContent .= '</a>';



			if($current_item_index % $columns_per_row == 0) $returnContent .= '[/one_third_last]';

			else $returnContent .= "[/one_third]";



			/*Increment*/

			$current_item_index++;

		endwhile;

		$returnContent .= '</div>';

		/*End and return rendered*/

		return do_shortcode($returnContent);

	}

	

?>
<?php

	/*Homepage Section 2 mini slider*/

	add_shortcode('Render_custom_mini_sldier', 'Render_custom_mini_sldier');

?>
<?php
	/*Sitemap*/
	add_shortcode('Render_siteMap', 'Render_siteMap');
	function Render_siteMap() {
		ob_start();
		?>
	<div class="siteMap">
		<h2 id="pages">Pages</h2>
		<ul id="siteMap_pages" style="visibility: hidden;">
			<?php
				// Add pages you'd like to exclude in the exclude here
				wp_list_pages(
					array(
						'exclude' => '',
						'title_li' => '',
					)
				);
			?>
		</ul>
		<script>
			<?php
				/*---- Get Pages to remove ----*/
				$args = array(
					'post_type' => 'page',
					'post_status' => 'publish'
				);
				$pagesToFilter = get_pages($args);
				$pagesToRemove = array();
				/*Filter them pages*/
				foreach ($pagesToFilter as $key => $page) {
					if(isset(get_field('exclude_page_from_sitemap', $page->ID)[0]) && get_field('exclude_page_from_sitemap', $page->ID)[0] == 1){
						$pagesToRemove[] = $page->ID;
					}
				}
			?>


			jQuery('ul#siteMap_pages').ready(function(){
				/*Show on ready*/
				jQuery('ul#siteMap_pages').css({"visibility": "visible"});

				/*Exclude pages from the list*/
				var toRemovePages = <?php echo json_encode($pagesToRemove); ?>;
				jQuery(toRemovePages).each(function(index, pageID){
					jQuery('ul#siteMap_pages li.page-item-' + pageID).remove();
				});
			})
		</script>

	</div>
		<?php
		$returnContent = ob_get_clean();

		return $returnContent;
	}
?>
<?php
	/*
		Convert text to Download link
	*/
	add_shortcode('SOA_download_link', 'SOA_download_link');
	function SOA_download_link($attr, $content){
		/*Validate*/
		if(isset($content) && $content != ''){
			/*init*/
			$link = get_bloginfo('template_url') . '/SOA-request-form-download.php';

			return do_shortcode("<a href='{$link}' target='_blank' download class='SOA-download-link'>{$content}</a>");
		}
	}
?>
<?php
	/*
		Show Secure-SOA-Request-Instructions
	*/
	add_shortcode('Secure_SOA_Request_Instructions', 'Secure_SOA_Request_Instructions');
	function Secure_SOA_Request_Instructions(){
		return do_shortcode(get_field('soa-request_custom_header_message', 776));
	}
?>
<?php















	/*============================================================ Global Use ============================================================*/



	/*This makes sure that the title to be return is the page and not of any other loop*/

	add_shortcode('Render_the_page_title', 'Render_the_page_title');

	function Render_the_page_title(){

		/*Get Current page ID*/

		global $dev_current_page_ID;



		/*End and return rendered*/

		return  get_the_title($dev_current_page_ID);

	}



	/*Social Icons*/

	add_shortcode('get_imageLoc_fb', 'get_imageLoc_fb');

	function get_imageLoc_fb(){

		return  get_bloginfo('template_url') . '/images/social-icons/facebook.png';

	}

	/*Social Icons*/

	add_shortcode('get_imageLoc_t', 'get_imageLoc_t');

	function get_imageLoc_t(){

		return  get_bloginfo('template_url') . '/images/social-icons/twitter.png';

	}

	/*Social Icons*/

	add_shortcode('get_imageLoc_g', 'get_imageLoc_g');

	function get_imageLoc_g(){

		return  get_bloginfo('template_url') . '/images/social-icons/googleplus.png';

	}

	/*Social Icons*/

	add_shortcode('get_imageLoc_in', 'get_imageLoc_in');

	function get_imageLoc_in(){

		return  get_bloginfo('template_url') . '/images/social-icons/linkedIn.png';

	}

	add_shortcode('parse_link_target', 'parse_link_target_shortcode');

	function parse_link_target_shortcode($attr, $link){

		return  parse_link_target(do_shortcode($link));

	}

?>