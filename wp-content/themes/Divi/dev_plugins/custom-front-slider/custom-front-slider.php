<?php

	/******** Outputs Slider for CPT ********/





	function Render_custom_sldier(){

		/*==== Get Slides ====*/

		$slider_query = new WP_Query(); 

		$slider_query->query('post_type=cpt_frontpage_slide&orderby=menu_order');

		$args = array(

			'posts_per_page'   => -1,

			'offset'           => 0,

			'orderby'          => 'menu_order',

			'order'            => 'ASC',

			'post_type'        => 'cpt_frontpage_slide',

			'post_status'      => 'publish',

			'suppress_filters' => true

		);

		$slides = get_posts( $args );

		$class = '';
		if(count($slides) < 2){
			$class = ' no_arrows ';
		}

		?>		



		<div id="main-slider" class="et_pb_slider et_slider_auto et_slider_speed_7000 et_pb_bg_layout_dark <?php echo count($slides); echo $class; ?>">

			<div class="et_pb_slides">

				<?php foreach($slides as $index=>$slide) : ?>

				<div class="et_pb_slide et_pb_bg_layout_dark et_pb_media_alignment_center <?php if($index == 0) echo 'et-pb-active-slide'; ?>" style="background-color: rgb(255, 255, 255); background-image: url(&quot;<?php echo get_field('slide_background_image', $slide->ID); ?>&quot;); z-index: 0; opacity: 0; display: none; <?php if($index == 0) echo 'z-index: 1; opacity: 1; display: block;';  ?> ">

					<div style="min-height: 550px;" class="et_pb_container clearfix">

						<div class="et_pb_slide_description">

							<h2><?php echo get_field('slide_text', $slide->ID); ?></h2>

							<div class="et_pb_slide_content">

							</div>

							<a href="<?php echo get_field('read_more_link', $slide->ID); ?>" class="et_pb_more_button">more</a>

						</div>

						<!-- .et_pb_slide_description -->

					</div>

					<!-- .et_pb_container -->

				</div>

				<!-- .et_pb_slide -->

				<?php endforeach; ?>

			</div>

			<!-- .et_pb_slides -->

			<div class="et-pb-slider-arrows">

				<a class="et-pb-arrow-prev" href="#"><span>Previous</span></a><a class="et-pb-arrow-next" href="#"><span>Next</span></a>

			</div>

			<div class="et-pb-controllers">

				<a href="#" class="">1</a><a class="et-pb-active-control" href="#">2</a><a class="" href="#">3</a>

			</div>

		</div>

		<!-- .et_pb_slider -->



		<?php

	}



?>