<?php	
    /**
     * Example Widget Class
     */
    class custom_sibling_page_list extends WP_Widget {


    /** constructor -- name this the same as the class above */
    function custom_sibling_page_list() {
      parent::WP_Widget(false, $name = 'List Siblings');  
    }
    
    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {
      /*Load pre-runtime variables*/
      global $dev_current_page_ID; //global $dev_current_page_ID; $dev_current_page_ID = get_the_ID();
      $activeClass = 'current-page';

      /*Decide if parent or not*/
      $current_post = get_post($dev_current_page_ID);
      if(isset($current_post) && $current_post->post_parent > 0){
        $parent_to_query = $current_post->post_parent;
      }
      else{
        $parent_to_query = $dev_current_page_ID;
      }

      $parent_page = get_post($parent_to_query);

      /*Runtime*/
      if(($dev_current_page_ID != null || $dev_current_page_ID != '')){
        if(get_post_type($dev_current_page_ID) == 'page'){
          /*List subpages/sibling pages for current page*/
          $args = array(
            'posts_per_page'   => -1,
            'offset'           => 0,
            'category'         => '',
            'orderby'          => 'menu_order',
            'order'            => 'ASC',
            'post_type'        => 'page',
            'post_parent'      => $parent_to_query,
            'post_status'      => 'publish',
            'suppress_filters' => true
          );

          $childPages = get_posts($args);
          ?>
          <div class="custom-sibling-pages">
            <div class="custom-sibling-pages-innerWrap">
              <ul class="menu custom-sibling-pages">
                <?php
                  $is_excluded = get_field('exclude_from_sidebar_page_list', $parent_page->ID);
                  if(!empty($is_excluded)) $is_excluded = $is_excluded[0];
                  else $is_excluded = 0;
                ?>
                <?php if($is_excluded != 1) : ?>
                <li class="parent-page <?php if($parent_page->ID == $dev_current_page_ID) echo ' ' . $activeClass . ' ';?>">
                  <a title="<?php echo $parent_page->post_title ?>" href="<?php echo get_permalink($parent_page->ID); ?>"><span><?php echo $parent_page->post_title ?></span></a>
                </li>
                <?php endif; ?>

                <?php foreach($childPages as $childPage) : ?>
                  <?php
                    $is_excluded = get_field('exclude_from_sidebar_page_list', $childPage->ID);
                    if(!empty($is_excluded)) $is_excluded = $is_excluded[0];
                    else $is_excluded = 0;
                  ?>
                  <?php if($is_excluded != 1) : ?>
                    <li class="sibling-page <?php if($childPage->ID == $dev_current_page_ID) echo ' ' . $activeClass . ' ';?>">
                      <a title="<?php echo $childPage->post_title ?>" href="<?php echo get_permalink($childPage->ID); ?>"><span><?php echo $childPage->post_title ?></span></a>
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>

              </ul>
            </div>
          </div>
          <?php
        }
      }
    }
    
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {
      return $old_instance;
    }
    
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {
      ?>
      <p>No settings required. This widget will only list the sibling pages (Sub pages of it's parent page) of the currently viewed page.</p>
      <?php
    }
    
    
    } // end class example_widget
    add_action('widgets_init', function(){
      register_widget("custom_sibling_page_list");
    });
?>