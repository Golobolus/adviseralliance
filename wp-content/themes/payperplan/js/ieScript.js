jQuery().ready(function(){

	/*---- Init ----*/

	function get_BG_URL(element){

		var pattern = /url\(|\)|"|'/g;

		var bgImage = element.css('backgroundImage');

		if(typeof bgImage != 'undefined' || bgImage != '' || bgImage != null bgImage = bgImage.replace(pattern,"");
		else bgImage = '';

		return bgImage;

	}



	function removeBG(element){

		element.css({backgroundImage:"none"});

	}



	function get_BG_color(element){

		return element.css("background-color");

	}



	function BG_fix2(element){

		var bgImage = get_BG_URL(jQuery(element));

		if(bgImage != 'none'){



		}

		element.prepend('<div class="bgFix" style="position: absolute; overflow: hidden; width: 100%; height: 100%;"><img class="position: absolute; overflow: hidden; fullBG-IE-fix" style="height: auto; width: auto; min-height: 100%; min-width: 100%;" src="' + bgImage + '" /></div>');

	}


	/*---- Runtime ----*/

	/*Sections*/
	var BG = get_BG_URL(jQuery('#paraplanning-content'));
	jQuery('#paraplanning-content').backstretch(BG);

	// jQuery('body.home .et_pb_section').each(function(index, element){

	// 	if(index > 2){

	// 		var BG = get_BG_URL(jQuery(element));

	// 		var BGcolor = get_BG_color(jQuery(element));

	// 		//BG_fix2(jQuery(element));

	// 		jQuery(element).backstretch(BG);

	// 		jQuery(element).backstretch(BG);

	// 		//removeBG(jQuery(element));

	// 	}

	// });

	/*Sidebar*/
	jQuery('.sibling-page a').backstretch('<?php bloginfo('template_url'); ?>/images/sidebar-menu-bg-hover.png');
	jQuery('.sibling-page').hover(
		function(){
			if(!jQuery(this).hasClass('current-page')) jQuery(this).children('a').children('div.backstretch').children('img').css({"visibility":"visible"});
		},
		function(){
			if(!jQuery(this).hasClass('current-page')) jQuery(this).children('a').children('div.backstretch').children('img').css({"visibility":"hidden"});
		}
	);

});


jQuery(document).ready(function(){
	setTimeout(function(){
		jQuery('.section3-mini-slider.initial-Slider-CSS').click(function(){
			jQuery(this).removeClass('initial-Slider-CSS');
		});
		jQuery('.section3-mini-slider.initial-Slider-CSS').mouseenter(function(){
			jQuery(this).removeClass('initial-Slider-CSS');
		});
	}, 100);
});