jQuery(function() {

  var scrollOffset = -68;

  jQuery('a[href*=#]:not([href=#])').click(function() {

    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

      var target = jQuery(this.hash);

      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');

      if (target.length) {

        jQuery('html,body').animate({

          scrollTop: target.offset().top + scrollOffset

        }, 800);

        return false;

      }

    }

  });

});

/*Back to top*/

jQuery(document).ready(function(){
  /*init*/
  var clicked = false;
  jQuery('#backToTop').hide();

  /*Back to top*/
  jQuery('#backToTop').click(function(){
    jQuery('html,body').animate({

      scrollTop: 0

    }, 800, function(){
      clicked = false
    });
  });

  /*Scroll detect*/
  jQuery(document).scroll(function(){
    var scrollTrigger = 400;
    var currentScroll = jQuery(document).scrollTop();

    if(!clicked){
      if(currentScroll >= scrollTrigger) jQuery('#backToTop').fadeIn(350);
      else jQuery('#backToTop').fadeOut(200);
    }

  });
  jQuery('#backToTop').click(function(){
    jQuery('#backToTop').fadeOut(200);
    clicked = true;
  });
});