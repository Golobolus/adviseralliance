	<footer id="main-footer">







		<?php get_sidebar( 'footer' ); ?>















		<div id="footer-bottom">







			<div class="container clearfix">







				<ul id="et-social-icons">







				<?php if ( 'on' === et_get_option( 'divi_show_facebook_icon', 'on' ) ) : ?>







					<li class="et-social-icon et-social-facebook">







						<a href="<?php echo esc_url( et_get_option( 'divi_facebook_url', '#' ) ); ?>">







							<span><?php esc_html_e( 'Facebook', 'Divi' ); ?></span>







						</a>







					</li>







				<?php endif; ?>







				<?php if ( 'on' === et_get_option( 'divi_show_twitter_icon', 'on' ) ) : ?>







					<li class="et-social-icon et-social-twitter">







						<a href="<?php echo esc_url( et_get_option( 'divi_twitter_url', '#' ) ); ?>">







							<span><?php esc_html_e( 'Twitter', 'Divi' ); ?></span>







						</a>







					</li>







				<?php endif; ?>







				<?php if ( 'on' === et_get_option( 'divi_show_google_icon', 'on' ) ) : ?>







					<li class="et-social-icon et-social-google">







						<a href="<?php echo esc_url( et_get_option( 'divi_google_url', '#' ) ); ?>">







							<span><?php esc_html_e( 'Google', 'Divi' ); ?></span>







						</a>







					</li>







				<?php endif; ?>







				<?php if ( 'on' === et_get_option( 'divi_show_rss_icon', 'on' ) ) : ?>







				<?php







					$et_rss_url = '' !== et_get_option( 'divi_rss_url' )







						? et_get_option( 'divi_rss_url' )







						: get_bloginfo( 'comments_rss2_url' );







				?>







					<li class="et-social-icon et-social-rss">







						<a href="<?php echo esc_url( $et_rss_url ); ?>">







							<span><?php esc_html_e( 'RSS', 'Divi' ); ?></span>







						</a>







					</li>







				<?php endif; ?>







				</ul>















				<p id="footer-info"><?php printf( __( 'Designed by %1$s | Powered by %2$s', 'Divi' ), '<a href="http://www.elegantthemes.com" title="Premium WordPress Themes">Elegant Themes</a>', '<a href="http://www.wordpress.org">WordPress</a>' ); ?></p>







			</div>	<!-- .container -->







		</div>







	</footer> <!-- #main-footer -->















	<?php wp_footer(); ?>















	<!-- Script loaded at bottom to decrease load time -->







	<script src="<?php bloginfo('template_url'); ?>/js/jquery.tooltipster.min.js"></script>







	<?php



		/*Import smooth scroll if not front page, else, use smooth scroll for front page. */



		if(!is_front_page()){



			include_customSmoothScroll();



		}



		else{







			include_customSmoothScroll_front();



		}



	?>







	<script>







		jQuery(document).ready(function(){







			/*======== Fix FAQ box sizes ========*/







			calculateFAQboxes();







			function calculateFAQboxes(){







				jQuery('.FAQ_row').each(function(index, element){







					/*---- Reset Height ----*/







					jQuery(element).find('.heading-more').each(function(){







						jQuery(this).height("auto");







					});















					/*---- Reconnaissance Collect data ----*/







					var maxHeight = 0;







					jQuery(element).find('.heading-more').each(function(){







						var currentHeight = jQuery(this).height();







						if(currentHeight > maxHeight) maxHeight = currentHeight;







					});















					/*---- Apply new uniform height----*/







					jQuery(element).find('.heading-more').each(function(){







						jQuery(this).height(maxHeight);







					});







				});







			}







			/*On Resize*/







			jQuery(window).resize(function(){







				calculateFAQboxes();







			});







			/*========= Fix FAQ box sizes ====END=*/







		});







	</script>











	<script>







		jQuery(document).ready(function(){







			/*init*/







			/*--SOA*/







			jQuery('.tooltip-enabled .ginput_container ul li:eq(0)').addClass('priceToolTip1');







			jQuery('.tooltip-enabled .ginput_container ul li:eq(1)').addClass('priceToolTip2');















			jQuery('#priceTable_tooltipTempContainer div.ptp-col:nth-child(1) > div:nth-child(2) .ptp-bullet-item').each(function(index, element){







				if(jQuery(element).html() == ''){







					jQuery(element).remove();







				}







			});







			/*Clean*/















			/*runtime*/







			var priceToolTip1_title = jQuery('#priceTable_tooltipTempContainer div.ptp-col:nth-child(1) > div:nth-child(2)').clone();







			var priceToolTip2_title = jQuery('#priceTable_tooltipTempContainer div.ptp-col:nth-child(2) > div:nth-child(2)').clone();







			jQuery('.priceToolTip1').tooltipster({







                content: jQuery(priceToolTip1_title),







                position: 'top',







                interactive: true







            });







			jQuery('.priceToolTip2').tooltipster({







                content: jQuery(priceToolTip2_title),







                position: 'top',







                interactive: true







            });







		});







	</script>























	<!--[if lt IE 9]>







	<script src="<?php bloginfo('template_url'); ?>/js/backStretch.js"></script>











	<script>



	jQuery(document).ready(function(){



		/*Sidebar*/



		jQuery('.sibling-page a').backstretch('<?php bloginfo('template_url'); ?>/images/sidebar-menu-bg-hover.png');



		jQuery('.sibling-page').hover(



			function(){



				if(!jQuery(this).hasClass('current-page')) jQuery(this).children('a').children('div.backstretch').children('img').css({"visibility":"visible"});



			},



			function(){



				if(!jQuery(this).hasClass('current-page')) jQuery(this).children('a').children('div.backstretch').children('img').css({"visibility":"hidden"});



			}



		);



	});



		



	jQuery(document).ready(function(){



		setTimeout(function(){



			jQuery('.section3-mini-slider.initial-Slider-CSS').click(function(){



				jQuery(this).removeClass('initial-Slider-CSS');



			});



			jQuery('.section3-mini-slider.initial-Slider-CSS').mouseenter(function(){



				jQuery(this).removeClass('initial-Slider-CSS');



			});



		}, 100);



	});



	</script>



	<![endif]-->



	



	<script>







	jQuery(document).ready(function(){



		jQuery('div.siteMap a').each(function(index, element){



			jQuery(element).attr('title', jQuery(element).text());



		});



	});



	</script>







	<script>



		/*Sticky Scroll-to-nav-bar*/



		jQuery(document).ready(function(){



			if(jQuery('#scrolling-menu').length > 0){



				/*init*/



				



				function enableEase(){



					// jQuery('#scrolling-menu').addClass('stickyEase');



					// jQuery('#scrolling-menu a').addClass('stickyEase');



				}



				



				function disableEase(){



					// jQuery('#scrolling-menu').removeClass('stickyEase');



					// jQuery('#scrolling-menu a').removeClass('stickyEase');



				}







				enableEase();



				var supportedWidth = 320;



				var restore;



				var timeToRestore = 0;



				var triggerOffset = 22;



				var initial_menuOffset = jQuery('#scrolling-menu .et_pb_row').offset().top;



				if(jQuery(window).width() == 767){



						triggerOffset = 0;



						if(jQuery('body.admin-bar').length > 0){



							triggerOffset = -22;



						}



					}



					else{



						triggerOffset = 22;



						if(jQuery('body.admin-bar').length > 0){



							triggerOffset = -15;



						}



					}



				var distanceTrigger = initial_menuOffset + triggerOffset;







				/*runtime*/



				jQuery(document, window).scroll(function(e){



					/*Detect Status*/



					var currentSroll = jQuery(document).scrollTop();







					/*Trigger When passing trigger*/



					if(currentSroll >= distanceTrigger){



						//enableEase();



						jQuery('#scrolling-menu').addClass('stickToTop');



						if(jQuery('#scrolling-menu').hasClass('holdPosition')) jQuery('#scrolling-menu').removeClass('holdPosition');



						clearTimeout(restore);



					}



					else{



						//disableEase();



						jQuery('#scrolling-menu').addClass('holdPosition');



						jQuery('#scrolling-menu').removeClass('stickToTop');



						jQuery('#scrolling-menu').css({"position":"absolute"});







						clearTimeout(restore);



						restore = setTimeout(function(){



							jQuery('#scrolling-menu').removeClass('holdPosition');



							jQuery('#scrolling-menu').css({"position":"relative"});



						}, timeToRestore);



					}



				});







				/*Recalculate on resize*/



				jQuery(window).resize(function(){



					/*Check Status*/



					var stickEnabled = jQuery('#scrolling-menu').hasClass('stickToTop');



					var holdEnabled = jQuery('#scrolling-menu').hasClass('holdPosition');



					//var easeEnabled = jQuery('#scrolling-menu').hasClass('stickyEase');







					/*init*/



					//disableEase();



					jQuery('#scrolling-menu').removeClass('stickToTop');



					jQuery('#scrolling-menu').removeClass('holdPosition');







					/*Recalcs*/



					initial_menuOffset = jQuery('#scrolling-menu .et_pb_row').offset().top;



					if(jQuery(window).width() <= 767){



						triggerOffset = -5;



						if(jQuery('body.admin-bar').length > 0){



							triggerOffset = -27;



						}



					}



					else{



						triggerOffset = 22;



						if(jQuery('body.admin-bar').length > 0){



							triggerOffset = -15;



						}



					}



					distanceTrigger = initial_menuOffset + triggerOffset;







					/*restore*/



					if(stickEnabled) jQuery('#scrolling-menu').addClass('stickToTop');



					if(holdEnabled) jQuery('#scrolling-menu').addClass('holdPosition');



					// if(easeEnabled) enableEase();







				});







				jQuery(window).trigger('resize');



			}



			



		});



	</script>







	<script>



		/*Stick nav bar Active on scroll*/



		jQuery(document).ready(function(){



			if(jQuery('#scrolling-menu').length > 0){



				/*---- Init ----*/



				/*Get positions of sections*/

				/* boxes */



				var Section_1_top = jQuery('#statement-advice-box').position().top;



				var Section_2_top = jQuery('#backoffice-support-box').position().top;



				var Section_3_top = jQuery('#benefits-outsourcing-box').position().top;



				var Section_4_top = jQuery('#get-started-box').position().top;



				var triggerOffset = -110;







				var previousSection = 0;



				var currentSection = 0;



				var maxScroll = jQuery(document).height() - jQuery(window).height();







				jQuery(document, window).scroll(function(){



					if(jQuery(document).scrollTop() >= (Section_4_top + triggerOffset - 300) || jQuery(document).scrollTop() >= maxScroll){



						// Section 4



						currentSection = 4;



					}



					else if(jQuery(document).scrollTop() >= (Section_3_top + triggerOffset)){



						// Section 3



						currentSection = 3;



					}



					else if(jQuery(document).scrollTop() >= (Section_2_top + triggerOffset)){



						// Section 2



						currentSection = 2;



					}



					else if(jQuery(document).scrollTop() >= (Section_1_top + triggerOffset)){



						// Section 1



						currentSection = 1;



					}



					else{



						currentSection = 0;



					}







					/*Performance fix: Recalculate only if on different section*/



					if(currentSection != previousSection){



						recalcStickyClasses();



					}



					function recalcStickyClasses(){



						/*Clean*/



						jQuery('#scrolling-menu .et_pb_row .et_pb_column .et_pb_text').removeClass('scrollActive');



						/* Button links! */



						switch(currentSection){



							case 1: {



								jQuery('#statement-advice').addClass('scrollActive');



								break;



							}



							case 2: {



								jQuery('#backoffice-support').addClass('scrollActive');



								break;



							}



							case 3: {



								jQuery('#benefits-outsourcing').addClass('scrollActive');



								break;



							}



							case 4: {



								jQuery('#get-started').addClass('scrollActive');



								break;



							}



							default: {



								break;



							}



						}







						previousSection = currentSection;



					}



				});











				/*Recalculate on resize*/



				jQuery(window).resize(function(){



					/*Check Status*/



					Section_1_top = jQuery('#statement-advice-box').position().top;



					Section_2_top = jQuery('#backoffice-support-box').position().top;



					Section_3_top = jQuery('#benefits-outsourcing-box').position().top;



					Section_4_top = jQuery('#get-started-box').position().top;



					maxScroll = jQuery(document).height() - jQuery(window).height();



				});



			}



		});



	</script>







	<script>



		/*For multifile uploader*/



		jQuery(document).ready(function(){



			/*Init variables*/



			var limit = 10;







			/*Check List for cancelled*/



			setInterval(function(){



				reviewFileList();



				recheckCanceled();



			}, 500);







			/*Runtime*/



			function reviewFileList(){



				var file_count = jQuery('#gform_preview_3_33 .ginput_preview').length;



				if(file_count >= limit){



					jQuery('#gform_browse_button_3_33').prop('disabled', 'disabled');



				}



				else{



					jQuery('#gform_browse_button_3_33').prop('disabled', '');



				}







				if(file_count > limit){



					jQuery('#gform_preview_3_33 .ginput_preview a').each(function(index, element){



						if(index + 1 <= file_count - limit){



							jQuery(element).click();



						}



					});



				}



			}







			function recheckCanceled(){



				jQuery("#gform_preview_3_33 .ginput_preview").each(function(index, element){



					var hasCancel = jQuery(element).children('b').length != 0;



					if(hasCancel){



						/*Recheck if trully cancelled*/



						var cancelled = jQuery(element).children('a').length == 0;



						if(cancelled){



							jQuery(element).remove();







							/*Remove max error*/



							var maxErrorFound = false;



							jQuery('#gform_multifile_messages_3_33 li').each(function(index, element){



								if(jQuery(element).text() == 'Maximum number of files reached'){



									if(maxErrorFound){



										jQuery(element).remove();



									}



									else maxErrorFound = true;



								}



							});



						}



					}



				});



			}



		});



		



	</script>











	<script>



		/*For repeater file uploader*/



		jQuery(document).ready(function(){



			jQuery('.add-more-docs-fallback .gsection_title').click(function(){



				/*init*/



				var fileInputCount = jQuery('.support-doc-fallback').length;



				var fileOpenedCounter = jQuery('#input_3_45');



				var fileOpenedCounter_val = Number(fileOpenedCounter.val());



				var newfileOpenedCounter_val = fileOpenedCounter_val +1;







				/*runtime*/



				if(newfileOpenedCounter_val <= fileInputCount){



					fileOpenedCounter.val(newfileOpenedCounter_val);



					jQuery('.support-doc-fallback').each(function(index, element){



						if(index == fileOpenedCounter_val){



							jQuery(element).css({"display":"block"});



						}



					});



				}



				if(newfileOpenedCounter_val >= fileInputCount) jQuery(this).parent().hide();







			});



		});



	</script>















	<?php



		/*Creates details object*/



		/*init*/



		$table_details = get_field('table_details', 981);



	?>



	<script>



		/*Definition to pricing tables*/



		/*init*/



		var table_details = <?php echo json_encode($table_details); ?>;







		/*Runtime*/



		jQuery(document).ready(function(){



			jQuery(table_details).each(function(index, table){



				/*init*/



				var tableShortcode = table.pricing_table_shortcode; //String



				var tableID = Number(tableShortcode.replace("[easy-pricing-table id=&quot;", '').replace("&quot;]", '')); //String



				var tableColumnDetails = table.table_details_to_add; //Array







				/*runtime*/



				jQuery(tableColumnDetails).each(function(index, columnDetail){



					if(columnDetail.details != ''){



						jQuery('#ptp-'+tableID+' .ptp-col-id-'+index+' .ptp-item-container').prepend('<div class="ptp-details">' + columnDetail.details + '</div>');



					}



					



				});



			});



		});







	</script>







	<script>



		/*Keep all Pricing tables details at the same height*/



		function alignPricingTables(){



			setTimeout(function(){



				var borderOffset = 2;//cHANGE THIS IF BORDER IS CHANGED!



				/*--- init ---*/



				/*functions*/



				function getMaxDetailsHeight(){



					var temp_maxHeight = 0;



					jQuery('.ptp-details').each(function(index, element){



						var currentheight = jQuery(element).outerHeight();



						if(currentheight > temp_maxHeight ){



							temp_maxHeight = currentheight;



						}



					})



					return temp_maxHeight;



				}



				/*vars*/



				var maxHeight = getMaxDetailsHeight();







				/*Runtime*/



				jQuery('.ptp-item-container').each(function(index, element){



					/*Holders*/



					var thisColumnDetails = jQuery(element).children('.ptp-details');



					var heightToAdd = maxHeight;







					/*---- Calculate partition if has Details ----*/



					if(thisColumnDetails.length > 0){	



						heightToAdd = heightToAdd - thisColumnDetails.outerHeight();



					}







					/*Process*/



					var topMargin = Number(heightToAdd) + Number(borderOffset);



					//jQuery(element).css({"position" : "relative", "margin-top": topMargin + "px"});



					jQuery(element).attr("style","position: relative !important; margin-top: " + topMargin + "px !important;");







				});



			}, 100);



		}







		jQuery(window).load(function(){alignPricingTables();});



		jQuery(window).resize(function(){alignPricingTables();});



	</script>











	<script>



		/*Gravity form error, remove error css on change and check emails*/



		jQuery(document).bind('gform_post_render', function(){



			/*Input*/



			jQuery('.gfield_error .ginput_container input, .gfield_error .ginput_container textarea').on('keydown', function(){



				if(jQuery(this).attr('id') != 'input_3_4') jQuery(this).closest('.gfield_error').removeClass('gfield_error');







				jQuery(this).closest('.ginput_container').siblings('.validation_message').remove();



			});







			/*select*/



			jQuery('.gfield_error .ginput_container .gfield_select, .gfield_radio li input').change(function(){



				jQuery(this).closest('.gfield_error').removeClass('gfield_error');



				jQuery(this).closest('.ginput_container').siblings('.validation_message').remove();



			});







			/*Email*/



			var email_pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;



			jQuery('#input_3_4, #input_2_2').on('input', function(){



				checkPattern(jQuery(this));



			});



			jQuery('#input_3_4, #input_2_2').on('keydown', function(){



				checkPattern(jQuery(this));



			});







			function validateEmail(email) { 



			    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;



			    return re.test(email);



			} 



			function checkPattern(input){



				if(validateEmail(input.val())){



					input.closest('.gfield_error').removeClass('gfield_error');



				}



				else input.closest('.gfield').addClass('gfield_error');



			}







			



		});



	</script>







	<!--[if lt IE 9]>



	<script>



		jQuery(document).ready(function(){



			var origText = jQuery('.gform_footer input.button.gform_button').attr('value');



			jQuery('.gform_footer').append('<input type="button" class="submitProxy" id="SOA_proxySubmit" value="'+origText+'" />');



			jQuery('.gform_footer input.button.gform_button').hide();



			



			jQuery('#SOA_proxySubmit').click(function(){



				var number_of_files = 0;



				jQuery('.support-doc-fallback .ginput_container input[type=file]').each(function(index, element){



					if(jQuery(this).val() == ''){



						number_of_files++;



					}



				});







				if(number_of_files == 10 && jQuery('.ginput_preview .gform_delete').length == 0){



					alert('Please add supporting documents.');



					return false;



				}



				else{



					jQuery('.gform_footer input.button.gform_button').click();



				}



			});



		});



	</script>



	<![endif]-->







	<!--<script src="<?php bloginfo('template_url'); ?>/js/ieScript.js"></script>-->

</body></html>