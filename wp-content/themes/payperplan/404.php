<?php get_header(); ?>



<div id="main-content">

	<div class="container">

		<div id="content-area" class="clearfix">

			<div id="left-area">

				<article id="post-0" <?php post_class( 'et_pb_post not_found' ); ?>>

					<?php get_template_part( 'includes/no-results', '404' ); ?>

					<?php 

						/*==== Load a Testimonial ====*/

						$temp = $wp_query; 

						$wp_query = null; 

						$wp_query = new WP_Query(); 

						$wp_query->query('showposts=1&post_type=cpt_testimonial&orderby=rand');

					 	while ($wp_query->have_posts()) : $wp_query->the_post(); 

					?>

						<!-- Output Testimonial -->

						<div id="testimonial_wrap">

							<div id="testimonial">

								<div id="quote-left"></div>

								<div id="quote-right"></div>

								<p class="testimonial_content"><?php echo get_the_content(); ?></p>

								<?php

									$authorString = get_field('testimonial_author', get_the_ID());

									if($authorString != '') :

								?>

									<p class="testimonial_author">- <?php echo $authorString; ?></p>

								<?php endif; ?>

							</div>

						</div>

					<?php endwhile; ?>

					<?php 

						$wp_query = null; 

						$wp_query = $temp;  // Reset

					?>

				</article> <!-- .et_pb_post -->

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>

		</div> <!-- #content-area -->

	</div> <!-- .container -->

</div> <!-- #main-content -->



<?php get_footer(); ?>