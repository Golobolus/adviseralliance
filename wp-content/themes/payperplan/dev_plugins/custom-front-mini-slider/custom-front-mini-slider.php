<?php

	/******** Outputs Slider for CPT ********/





	function Render_custom_mini_sldier(){

		/*==== Get Slide images ====*/

		$slide_images = get_field('section_2_mini-slider_images', 292);



		/*Settings*/

		$images_per_slide = 3;

		$images_len = count($slide_images);



		/*Init*/

		$returnContent = '';

		$currentImageIndex = 1;



		/*Runtime*/

		ob_start();

		?>

		<div class="et_pb_slider et_pb_slider_fullwidth_off et_slider_auto et_slider_speed_5000 section3-mini-slider et_pb_bg_layout_dark initial-Slider-CSS">

			<div class="et_pb_slides">



				<?php foreach ($slide_images as $slide_image) : ?>

					<?php if($currentImageIndex % $images_per_slide == 1 || $currentImageIndex == 1) : //et-pb-active-slide?>

					<?php

						$slideStyle = 'background-color: #transparent; z-index: 2; opacity: 0; display: none;';

						if($currentImageIndex == 1) $slideStyle = 'background-color: #transparent; z-index: 1; opacity: 1; display: block;';

					?>

					<div class="et_pb_slide et_pb_bg_layout_dark et_pb_media_alignment_center " style="<?php echo $slideStyle; ?>">

						<div class="et_pb_container clearfix">

							<div class="et_pb_slide_description">

								<div class="et_pb_slide_content">

									<ul>

					<?php endif; ?>

										<li><img class="alignnone size-full wp-image-105" src="<?php echo $slide_image['image']['sizes']['thumbnail']; ?>" alt="<?php echo $slide_image['image']['title']; ?>" width="175" height="175" /></li>

					<?php if(($currentImageIndex > 2) && ($currentImageIndex % $images_per_slide == 0) || $images_len == $currentImageIndex) : ?>

									</ul>

								</div>

							</div>

							<!-- .et_pb_slide_description -->

						</div>

						<!-- .et_pb_container -->

					</div>

					<!-- .et_pb_slide -->

					<?php endif; ?>

					<?php $currentImageIndex++ ?>

				<?php endforeach; ?>





			</div>

			<!-- .et_pb_slides -->

		</div>



		<?php

		$returnContent = ob_get_clean();

		return $returnContent;

	}



?>