<?php
	/*======== CPT: Add Columns for Sliders ========*/

	/**
	* add order column to admin listing screen for header text
	*/
	function add_new_frontpage_slide_column($frontpage_slide_columns) {
	  $frontpage_slide_columns['menu_order'] = "Order";
	  return $frontpage_slide_columns;
	}
	add_action('manage_edit-cpt_frontpage_slide_columns', 'add_new_frontpage_slide_column');


	/**
	* show custom order column values
	*/
	function show_order_column($name){
	  global $post;

	  switch ($name) {
	    case 'menu_order':
	      $order = $post->menu_order;
	      echo $order;
	      break;
	   	default:
	      break;
	   }
	}
	add_action('manage_cpt_frontpage_slide_posts_custom_column','show_order_column');

	/**
	* make column sortable
	*/
	function order_column_register_sortable($columns){
	  $columns['menu_order'] = 'menu_order';
	  return $columns;
	}
	add_filter('manage_edit-cpt_frontpage_slide_sortable_columns','order_column_register_sortable');

?>