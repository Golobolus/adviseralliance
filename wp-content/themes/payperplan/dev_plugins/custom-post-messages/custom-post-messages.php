<?php
  /*======== CPT MESSAGES for: cpt_i_contact ========*/
  add_filter('post_updated_messages', 'cpt_i_contact_messages');
  function cpt_i_contact_messages( $messages ) {
    global $post, $post_ID;

    $messages['cpt_i_contact'] = array(
      0 => '', // Unused. Messages start at index 1.
      1 => sprintf( __('Contact Page Settings Updated') ),
      2 => __('Custom field updated.'),
      3 => __('Custom field deleted.'),
      4 => __('Contact Page Settings Updated.'),
      /* translators: %s: date and time of the revision */
      5 => isset($_GET['revision']) ? sprintf( __('Book restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
      6 => sprintf( __('Contact Page Settings Updated.') ),
      7 => __('Contact Page Settings Updated.'),
    );

    return $messages;
  }

  /*======== CPT MESSAGES for: Settings - This idea came in late to mind ========*/
  add_filter('post_updated_messages', 'cpt_set_messages');
  function cpt_set_messages( $cpt_messages ) {
    $post_types = get_post_types();

    foreach ($post_types as $key => $value) {
      if(substr( $key, 0, 8 ) === 'cpt_set_'){
        $cpt_messages[$key] = array(
          0 => '', // Unused. Messages start at index 1.
          1 => __('Changes Saved.'),
          2 => __('Changes Saved.'),
          3 => __('Changes Saved.'),
          4 => __('Changes Saved.'),
          5 => __('Changes Saved.'),
          6 => __('Changes Saved.'),
          7 => __('Changes Saved.'),
        );
      }
    }
    return $cpt_messages;
  }

?>