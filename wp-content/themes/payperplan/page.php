<?php



get_header();



$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );



$current_page_ID = get_the_ID();



?>

<?php if(is_front_page()){ ?>
	<div id="backToTop"></div>
<?php } ?>
<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	


	<div class="container">
		<?php if(function_exists('bcn_display') && !is_front_page()) { ?>
		<div class="breadcrumbs">
			<?php bcn_display(); ?>
		</div>
		<?php } ?>
		<div id="content-area" class="clearfix">



			<div id="left-area">



<?php endif; ?>



			<?php while ( have_posts() ) : the_post(); ?>

				<?php global $dev_current_page_ID; $dev_current_page_ID = get_the_ID(); ?>



				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



				<?php if ( ! $is_page_builder_used ) : ?>



					<!--<h1 class="main_title"><?php the_title(); ?></h1>-->

				<?php

					$thumb = '';



					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );



					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );

					$classtext = 'et_featured_image';

					$titletext = get_the_title();

					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );

					$thumb = $thumbnail["thumb"];



					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )

						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );

				?>



				<?php endif; ?>



					<div class="entry-content">



					<?php

						if (is_front_page()) {

							Render_custom_sldier();

						}

					?>

					<?php

						the_content();



						if ( ! $is_page_builder_used )

							wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Divi' ), 'after' => '</div>' ) );

					?>

					</div> <!-- .entry-content -->





				<?php

					/*==== Show I AM READY TO GO ====*/

					$go_toggle = get_field('show_go', $current_page_ID);

					if(isset($go_toggle[0]) && $go_toggle[0] == 1) :



				?>

					<div id="I_am_ready_to_go">

						<div class="title"><?php echo et_get_option('dev_custom-go_section_header'); ?></div>

						<div class="instructions_text"><?php echo et_get_option('dev_custom-info_text'); ?></div>

						<div class="buttons-container">

							<div class="buttonLeft">

								<?php $l_link =  et_get_option('dev_custom-l_button_target');?>

								<a href="<?php echo et_get_option('dev_custom-l_button_target'); ?>" target="<?php echo parse_link_target(et_get_option('dev_custom-l_button_target')); ?>"><?php echo et_get_option('dev_custom-l_button_text'); ?></a>

							</div>

							<div class="buttonRight">

								<a href="<?php echo et_get_option('dev_custom-r_button_target'); ?>" target="<?php echo parse_link_target(et_get_option('dev_custom-r_button_target')); ?>"><?php echo et_get_option('dev_custom-r_button_text'); ?></a>

							</div>

						</div>

					</div>

					<?php endif; ?>



				<?php

					/*==== Testimonials Toggle ====*/

					$testimonials_toggle = get_field('show_testimonials', $current_page_ID);

					if(!isset($testimonials_toggle[0]) || $testimonials_toggle[0] != 1) :

				?>

					<?php 

						/*==== Load a Testimonial ====*/

						$temp = $wp_query; 

						$wp_query = null; 

						$wp_query = new WP_Query(); 

						$wp_query->query('showposts=1&post_type=cpt_testimonial&orderby=rand');

					 	while ($wp_query->have_posts()) : $wp_query->the_post(); 

					?>



						<!-- Output Testimonial -->

						<div id="testimonial_wrap">

							<div id="testimonial">

								<div id="quote-left"></div>

								<div id="quote-right"></div>

								<p class="testimonial_content"><?php echo get_the_content(); ?></p>

								<?php

									$authorString = get_field('testimonial_author', get_the_ID());

									if($authorString != '') :

								?>

									<p class="testimonial_author">- <?php echo $authorString; ?></p>

								<?php endif; ?>

							</div>

						</div>



					<?php endwhile; ?>

					<?php 

						$wp_query = null; 

						$wp_query = $temp;  // Reset

					?>



				<?php endif; ?>









				<?php

					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );

				?>



				</article> <!-- .et_pb_post -->



			<?php endwhile; ?>



				



<?php if ( ! $is_page_builder_used ) : ?>



			</div> <!-- #left-area -->



			<?php get_sidebar(); ?>

		</div> <!-- #content-area -->

	</div> <!-- .container -->



<?php endif; ?>



</div> <!-- #main-content -->



<?php get_footer(); ?>