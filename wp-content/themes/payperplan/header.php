<?php if ( ! isset( $_SESSION ) ) session_start(); ?>



<!DOCTYPE html>



<!--[if IE 6]>



<html id="ie6" <?php language_attributes(); ?>>



<![endif]-->



<!--[if IE 7]>



<html id="ie7" <?php language_attributes(); ?>>



<![endif]-->



<!--[if IE 8]>



<html id="ie8" <?php language_attributes(); ?>>



<![endif]-->



<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->



<html <?php language_attributes(); ?>>



<!--<![endif]-->



<head>



<meta name="google-site-verification" content="6BNXeoT7a6hx7luYYTYWorbXpoOl-OyQLPx6KirnxpI" />



	<meta charset="<?php bloginfo( 'charset' ); ?>" />



	<title><?php elegant_titles(); ?></title>



	<?php elegant_description(); ?>



	<?php elegant_keywords(); ?>



	<?php elegant_canonical(); ?>







	<?php do_action( 'et_head_meta' ); ?>







	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />







	<?php $template_directory_uri = get_template_directory_uri(); ?>



	<style type="text/css">

		body.safari #scrolling-menu.stickToTop .et_pb_column_1_4 {

			width: 220px;

			margin: 0px;

		}

	</style>



	<!--[if lt IE 9]>

	

	<style type="text/css">

		#backToTop {

			display: block !important;

		}

	</style>





	<script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>



	<![endif]-->







	<!-- Dynamic stylesheet -->



	<style>



		<?php $bg = get_field('section_bg', 291); if(strlen($bg) > 0) : ?>



			div.entry-content > div.et_pb_section#paraplanning-content{



				background-image: url("<?php echo $bg; ?>") !important;



			}



		<?php endif; ?>







		<?php $bg = get_field('section_bg', 292); if(strlen($bg) > 0) : ?>



			div.entry-content div.et_pb_section#quality-service2{



				background-image: url("<?php echo $bg; ?>") !important;



			}



		<?php endif; ?>







		<?php $bg = get_field('section_bg', 293); if(strlen($bg) > 0) : ?>



			div.entry-content div.et_pb_section#soa2{



				background-image: url("<?php echo $bg; ?>") !important;



			}



		<?php endif; ?>







		<?php $bg = get_field('section_bg', 294); if(strlen($bg) > 0) : ?>



			div.entry-content div.et_pb_section#get-started-box{



				background-image: url("<?php echo $bg; ?>") !important;



			}



		<?php endif; ?>











	</style>











	<script type="text/javascript">



		document.documentElement.className = 'js';



	</script>







	<?php wp_head(); ?>



	<?php include_ACFgMap(); ?>



	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/tooltipser-master/tooltipster.css" type="text/css" media="screen" />



	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/custom-style.css" type="text/css" media="screen" />



	<?php if(is_front_page()) : ?>



	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/custom-frontpage-style.css" type="text/css" media="screen" />



	<style>



		/*==== Pricing table dynamic customization ====*/



		.ptp-most-popular {



		    color: <?php echo get_field('best_offer_color', 293); ?> !important;



		}



		/*==== Pricing table dynamic customization END=*/



	</style>



	<?php endif; ?>







	<?php if(!is_front_page()) : ?>



	<style>



		#main-header { padding-bottom: 0px; }



	</style>



	<?php endif; ?>



	<style>



		/*==== SOA Request Form ====*/



		<?php $trigger = get_field('show_soa_request_form_download', 776); ?>



		<?php if(isset($trigger[0]) && $trigger[0] == 1) : ?>



		.SOA_download_form_container {



			display: block !important;



		}



		<?php endif; ?>



		/*==== SOA Request Form END=*/



	</style>



	<!--[if lt IE 10]>

		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie10-lt.css" type="text/css" media="screen" />

	<![endif]-->



	<!--[if lt IE 9]>



		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie8.css" type="text/css" media="screen" />



	<![endif]-->



	<!--[if lt IE 9]> 

		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

	<![endif]-->



	<?php if(wp_is_mobile()) { ?>

		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/mobile.css" type="text/css" media="screen" />

	<?php } ?>



</head>



<body <?php body_class(); ?>>



	<header id="main-header">



		<div class="container clearfix">



		<?php



			$logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo



				? $user_logo



				: $template_directory_uri . '/images/logo.png';



		?>



			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logobox">



				<img src="<?php echo esc_attr( $logo ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo" />



			</a>



			



			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('header') ) : ?><?php endif ; ?>



			



			<div id="et-top-navigation">



				<nav id="top-menu-nav">



				<?php



					$menuClass = 'nav';



					if ( 'on' == et_get_option( 'divi_disable_toptier' ) ) $menuClass .= ' et_disable_top_tier';



					$primaryNav = '';







					$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false ) );







					if ( '' == $primaryNav ) :



				?>



					<ul id="top-menu" class="<?php echo esc_attr( $menuClass ); ?>">



						<?php if ( 'on' == et_get_option( 'divi_home_link' ) ) { ?>



							<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>



						<?php }; ?>







						<?php show_page_menu( $menuClass, false, false ); ?>



						<?php show_categories_menu( $menuClass, false ); ?>



					</ul>



				<?php



					else :



						echo( $primaryNav );



					endif;



				?>



				</nav>







				<?php do_action( 'et_header_top' ); ?>



			</div> <!-- #et-top-navigation -->



		</div> <!-- .container -->



	</header> <!-- #main-header -->